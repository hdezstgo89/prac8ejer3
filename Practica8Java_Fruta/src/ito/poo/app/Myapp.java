package ito.poo.app;

import ito.poo.clases.Fruta;
import ito.poo.clases.Periodo;

@SuppressWarnings("unused")
public class Myapp {

	
	static void run() {
		Fruta f1 = new Fruta("Manzana", 6F, 10, 100);
		System.out.println(f1);
		System.out.println();
		f1.agregarPeriodo("Un mes", 4000);
		System.out.println(f1);
		System.out.println();

		Fruta f2 = new Fruta("Pera", 10F, 20, 200);
		System.out.println(f2);
		System.out.println();
		f2.agregarPeriodo("Dos meses", 3000);
		System.out.println(f2);
		System.out.println();
		
		Fruta f3 = new Fruta("Manzana", 15F, 30, 300);
		System.out.println(f3);
		System.out.println();
		f3.agregarPeriodo("Tres meses", 4000);
		System.out.println(f3);
		System.out.println();
	}
	
	static void run2() {
		Fruta f1 = new Fruta("Manzana", 6F, 10, 100);
		System.out.println(f1);
		System.out.println();
		f1.agregarPeriodo("Febrero", 4000);
		f1.agregarPeriodo("Junio y Julio", 7500);
		System.out.println(f1);
		System.out.println();
		System.out.println(f1.getPeriodos());
		System.out.println(f1.devolverPeriodo(3));
		System.out.println(f1.devolverPeriodo(1));
		System.out.println();
		System.out.println(f1.costoPeriodo(f1.devolverPeriodo(3)));
		System.out.println(f1.costoPeriodo(f1.devolverPeriodo(1)));
		System.out.println(f1.gananciaEstimada(f1.devolverPeriodo(1)));

		System.out.println();
		System.out.println("/************************************************************************/");
		System.out.println();

		Fruta f2 = new Fruta("Pera", 10F, 20, 200);
		System.out.println(f2);
		System.out.println();
		f2.agregarPeriodo("Marzo", 3000);
		f2.agregarPeriodo("Abril y Mayo", 6550);
		System.out.println(f2);
		System.out.println();
		System.out.println(f2.getPeriodos());
		System.out.println(f2.devolverPeriodo(3));
		System.out.println(f2.devolverPeriodo(1));
		System.out.println();
		System.out.println(f2.costoPeriodo(f2.devolverPeriodo(3)));
		System.out.println(f2.costoPeriodo(f2.devolverPeriodo(1)));
		System.out.println(f2.gananciaEstimada(f2.devolverPeriodo(1)));

		System.out.println();
		System.out.println("/************************************************************************/");
		System.out.println();
		
		Fruta f3 = new Fruta("Manzana", 15F, 30, 300);
		System.out.println(f3);
		System.out.println();
		f3.agregarPeriodo("Febrero", 1000);
		f3.agregarPeriodo("Junio y Julio", 2500);
		System.out.println(f3);
		System.out.println();
		System.out.println(f3.getPeriodos());
		System.out.println(f3.devolverPeriodo(3));
		System.out.println(f3.devolverPeriodo(1));
		System.out.println();
		System.out.println(f3.costoPeriodo(f3.devolverPeriodo(3)));
		System.out.println(f3.costoPeriodo(f3.devolverPeriodo(1)));
		System.out.println(f3.gananciaEstimada(f3.devolverPeriodo(1)));
		System.out.println();
		
		System.out.println(!f1.equals(f2));
	    System.out.println(f2.compareTo(f1));
	    
	    System.out.println(!f1.equals(f3));
	    System.out.println(f3.compareTo(f1));
	}

	public static void main(String[] args) {
		run2();
	}
}
