package ito.poo.clases;

import java.util.ArrayList;

public class Fruta {

	private String nombre;
	private float extension;
	private float costoPromedio;
	private float precioVentaProm;
	private ArrayList<Periodo> periodos = new ArrayList<Periodo>();
	/*************************************************************************************/
	public Fruta() {
		super();
	}

	public Fruta(String nombre, float extension, float costoPromedio, float precioVentaProm) {
		super();
		this.nombre = nombre;
		this.extension = extension;
		this.costoPromedio = costoPromedio;
		this.precioVentaProm = precioVentaProm;
	}
	/*************************************************************************************/
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	private float getExtencion() {
		return extension;
	}

	public void setExtension(float extension) {
		this.extension = extension;
	}

	public float getCostoPromedio() {
		return costoPromedio;
	}

	public void setCostoPromedio(float costoPromedio) {
		this.costoPromedio = costoPromedio;
	}

	public float getPrecioVentaProm() {
		return precioVentaProm;
	}

	public void setPrecioVentaProm(float precioVentaProm) {
		this.precioVentaProm = precioVentaProm;
	}

	public ArrayList<Periodo> getPeriodos() {
		return this.periodos;
	}
	/*************************************************************************************/
	public void agregarPeriodo(String tiempoCosecha, float cantCosechaxtiempo) {
		Periodo p = new Periodo (tiempoCosecha, cantCosechaxtiempo);
			this.periodos.add(p);
	}
	
	public Periodo devolverPeriodo(int i) {
		Periodo dP;
			if (i > this.periodos.size() || i < 0)
				dP = new Periodo ("null", 0);
			else 
				dP = this.periodos.get(i);
			return dP;
	}

	public boolean eliminarPeriodo(int i) {
		boolean eP;
		if (i > this.periodos.size() || i < 0)
			eP = false;
		else {
			this.periodos.remove(i);
			eP = true;
		}
		return eP;
	}
	
	public float costoPeriodo(Periodo p) {
		float i = 0;
			if (i > this.periodos.size() || i < 0)
				i = 0;
			else 
				i = p.getCantCosechaXTiempo() * this.costoPromedio;
			return i;
	}

	public float gananciaEstimada(Periodo p) {
		float i = 0;
			if (i > this.periodos.size() || i < 0)
				i = 0;
			else 
				i = (p.getCantCosechaXTiempo() * this.precioVentaProm) - costoPeriodo(p);;
			return i;
	}
	/*************************************************************************************/
	@Override
	public String toString() {
		return "Fruta: " + nombre + "\nExtension: " + extension + "\nCosto Promedio: " + costoPromedio
				+ "\nPrecio Venta Promedio: " + precioVentaProm + "\nPeriodos: " + periodos;
	}
	/*************************************************************************************/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(costoPromedio);
		result = prime * result + Float.floatToIntBits(extension);
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((periodos == null) ? 0 : periodos.hashCode());
		result = prime * result + Float.floatToIntBits(precioVentaProm);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fruta other = (Fruta) obj;
		if (Float.floatToIntBits(costoPromedio) != Float.floatToIntBits(other.costoPromedio))
			return false;
		if (Float.floatToIntBits(extension) != Float.floatToIntBits(other.extension))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (periodos == null) {
			if (other.periodos != null)
				return false;
		} else if (!periodos.equals(other.periodos))
			return false;
		if (Float.floatToIntBits(precioVentaProm) != Float.floatToIntBits(other.precioVentaProm))
			return false;
		return true;
	}
	
	public int compareTo(Fruta arg0) {
		int r=0;
		if (!this.nombre.equals(arg0.getNombre()))
			return this.nombre.compareTo(arg0.getNombre());
		else if (this.extension != arg0.getExtencion())
			return this.extension>arg0.getExtencion()?1:-1;
		else if (this.precioVentaProm != arg0.getPrecioVentaProm())
			return this.precioVentaProm>arg0.getPrecioVentaProm()?2:-2;
		else if (this.costoPromedio != arg0.getCostoPromedio())
			return this.costoPromedio>arg0.getCostoPromedio()?3:-3;
		return r;
	}
}